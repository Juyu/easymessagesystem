﻿using System.Collections;
using System.Collections.Generic;
using EasyMessageSystem;
using UnityEngine;

public class Demo : MonoBehaviour {
	private int Number;
	public void Click () {
		Number++;
		EventManager.Instance.Notify("ShowNumber", new EventMessage(Number));
	}
}
