﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyMessageSystem;
public class NumberText : MonoBehaviour {
	private Text Number;

	void Awake()
	{
		Number = GetComponent<Text>();
		EventManager.Instance.Register("ShowNumber", UpdateText);
	}

	void OnDestroy()
	{
		EventManager.Instance.Unregister("ShowNumber", UpdateText);
	}

	private void UpdateText(EventMessage msg){
		Number.text = msg.GetParam<int>().ToString();
	}
	
}
