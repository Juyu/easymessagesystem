﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 一个简单的事件消息分发系统，通过字串符检索事件注册Action，当事件发生的时候，会调用相应的Action。

namespace EasyMessageSystem{
	public class EventManager {
		// 单例模式，通过EasyMessageSystem.EventManager.instance访问。
		public static EventManager Instance;
		private static Dictionary<string, Action<EventMessage>> eventListeners;

		static EventManager(){
			Instance = new EventManager();
			eventListeners = new Dictionary<string, Action<EventMessage>>();
		}

		//通过字串符检索事件并注册Action，在Awake调用。
		public void Register(string eventName, Action<EventMessage> action){
			if(!eventListeners.ContainsKey(eventName)){
				eventListeners.Add(eventName, action);
			}else{
				//保证委托唯一性
				eventListeners[eventName] -= action;
				eventListeners[eventName] += action;
			}
		}
		//通过字串符检索事件并取消注册，在OnDestroy调用。
		public void Unregister(string eventName, Action<EventMessage> action){
			if(eventListeners.ContainsKey(eventName)){
				eventListeners[eventName] -= action;
			}
		}

		//事件发生的时候调用，msg可以为空，也可以携带消息内容，并在消息内部携带参数。
		public void Notify(string eventName, EventMessage msg = null){
			if(eventListeners.ContainsKey(eventName)){
				Action<EventMessage> action = eventListeners[eventName];
				if(action != null) action(msg);
			}
		}
	}

	//notify的时候携带的消息类型，param可携带任意类型的数据。
	public sealed class EventMessage{
		public System.Object param;
		public EventMessage(){}
		public EventMessage(System.Object p){
			this.param = p;
		}
		public T GetParam<T>(){
			if(param == null) return default(T);
			return (T) param;
		}
	}

}
