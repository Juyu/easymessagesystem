﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace EasyMessageSystem{
	public class MessageSystemBehaviour : MonoBehaviour {
		private Dictionary<string, Action<EventMessage>> events = new Dictionary<string, Action<EventMessage>>();
		public void RegisterEvent(string eventName, Action<EventMessage> action){
			EasyMessageSystem.EventManager.Instance.Register(eventName, action);
			events.Add(eventName, action);
		}

		void OnDestroy()
		{
			foreach(string eventName in events.Keys){
				EasyMessageSystem.EventManager.Instance.Unregister(eventName, events[eventName]);
			}
		}

	}



}