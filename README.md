# EasyMessageSystem
EasyMessageSystem是一个轻量级的Unity开发解耦工具，核心代码不到50行。它主要通过消息分发的机制，对各个模块之间进行解耦。

## 应用场景
假设有模块：
- A：游戏控制逻辑模块；
- B：游戏界面控制模块；
- C：广告控制模块；
- D：游戏统计模块；

需求：
- 在游戏胜利的时候，需要弹出胜利界面。

那么普通实现是通过在A里面直接引用B并调用B方法。

后来新增需求：
- 在游戏胜利的时候弹广告；
- 在游戏胜利的时候发送统计；

此时我们需要再次在A里面引用模块C和D，并调用对应的方法。

### 不足之处

1. 代码耦合性太强：A对BCD模块有直接的引用；
2. 不利于多人开发：如果ABCD四个模块由不同的人开发，那么BCD需要提前跟A商定好接口，如果接口改动，那么A还要跟着更改；
3. 不利于维护与功能增加：如果后期有E模块，那么还要在A模块里加入E模块引用并修改A模块的代码；

### 更好的方法

在游戏胜利的时候发出游戏胜利的消息，让其他模块监听该消息即可。

例如游戏胜利的时候，A发出胜利的消息。

B模块监听该消息，并在胜利消息发生的时候弹窗。

假设后来需要在胜利的时候弹广告、发送统计，那么直接让相应的模块监听该事件即可，不需要在A里面进行修改，A也不需要直接引用到B、C、D，从而达到解耦的目的。

## 入门

```csharp
void Awake()
{
    // 注册事件并提供回调
    EventManager.Instance.Register("EventName", OnEvent);
}

private void OnEvent(EasyMessageSystem.EventMessage msg){
    Debug.Log("OnEvent " + msg.GetParam<string>());
}

void OnDestroy()
{
    // 取消注册事件
    EventManager.Instance.Unregister("EventName", OnEvent);
}

```

```csharp
// 事件发生，并携带参数
EventManager.Instance.Notify("EventName", new EventMessage("Hello."));
```
## 接口

### EventManager

#### void Register(string eventName, Action<EventMessage> action)
通过字串符检索事件并注册Action。
建议在Awake调用。
- eventName:事件ID；
- action:事件回调，带参数EventMessage；

```csharp
EventManager.Instance.Register("EventName", OnEvent);
```

#### void Unregister(string eventName, Action<EventMessage> action)
通过字串符检索事件并取消注册。
建议在OnDestroy调用。
- eventName:事件ID；
- action:事件回调，带参数EventMessage；

```csharp
EventManager.Instance.Unregister("EventName", OnEvent);
```

#### void Notify(string eventName, EventMessage msg = null)
事件发生的时候调用，可以携带消息内容，并在消息内部携带参数。
- eventName:事件ID；
- EventMessage:事件消息，非必须的参数；

```csharp
EventManager.Instance.Notify("EventName");

EventManager.Instance.Notify("EventName", new EventMessage("Hello."));
```

### EventMessage
消息类，可通过该类携带参数，监听事件的模块可通过内部方法获取消息以及参数。
```csharp
// 如果需要携带多个参数，可使用Dictionary；
EventMessage msg = new EventMessage("Hello.");
EventManager.Instance.Notify("EventName", msg);
```

```csharp
private void OnEvent(EasyMessageSystem.EventMessage msg){
    Debug.Log("OnEvent " + msg.GetParam<string>());
}
```

### MessageSystemBehaviour
继承自MonoBehaviour，会自动Unregister，但前提是通过RegisterEvent注册事件，

```csharp
using EasyMessageSystem;
public class NewBehaviourScript : MessageSystemBehaviour {
    void Awake()
    {
        RegisterEvent("EventName", OnEvent);
    }
    
    private void OnEvent(EventMessage msg){
    
    }
}
```
